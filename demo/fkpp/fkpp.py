# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np
from dolfin import *

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["optimize"] = True

T = Constant(0.1)
k_inv = Constant(1000.0)
sigma = Constant(0.2)
c = Constant(50.0)

snapshot_mesh = UnitSquareMesh(256, 256, "crossed")
snapshot_mesh.coordinates()[:] *= 3.0

coarse_mesh = UnitSquareMesh(2, 2)
coarse_mesh.coordinates()[:] *= 3.0

V = FunctionSpace(snapshot_mesh, "CG", 1)

u_0 = Expression("exp(-((x[0] - x_c)*(x[0] - x_c) + (x[1] - y_c)*(x[1] - y_c))/(sigma*sigma))",
                 x_c=0.0, y_c=0.0, sigma=sigma, degree=3)
all_boundary = CompiledSubDomain("on_boundary")


def assemble_inner_product(V=V):
    u = TrialFunction(V)
    v = TestFunction(V)

    a = inner(u, v) * dx
    M = assemble(a)

    return M


def a(u, v):
    a = (k_inv - c / 2.0) * inner(u, v) * dx + inner(grad(u), grad(v)) * dx
    return a


def L(u, v):
    L = (k_inv + c / 2.0) * inner(u, v) * dx - c * inner(u**2, v) * dx
    return L


def fkpp(x_c, y_c, id="test", V=V, ts=None, write_xdmf=False, hdf5_handle=None):
    u = TrialFunction(V)
    v = TestFunction(V)

    u_0.x_c = x_c
    u_0.y_c = y_c
    u_h = interpolate(u_0, V)

    a_form = a(u, v)
    L_form = L(u_h, v)

    bc = DirichletBC(V, Constant(0.0), all_boundary)

    assembler = SystemAssembler(a_form, L_form, bc)
    # Assemble once, not a function of current solution.
    A = Matrix()
    assembler.assemble(A)
    
    t = np.array([0.0])
    i = 0
    b = Vector()
    # To store next solution in time.
    u_h_ = Function(V)
    # For output as we enter timestepping loop.
    u_h_.assign(u_h)

    solver = PETScKrylovSolver("cg", "amg")
    solver.set_operator(A)

    # Step forward in time.
    while t < T.values() - DOLFIN_EPS:
        # Output results from previous timestep
        if write_xdmf:
            XDMFFile("output/%s/u_%s.xdmf" %
                     (str(id), str(i).zfill(4))).write(u_h_)
        if hdf5_handle is not None:
            name = "snapshots-%s-%s" % (id, str(i).zfill(4))
            if ts is not None:
                if (np.abs(t - ts) < DOLFIN_EPS_LARGE).any():
                    hdf5_handle.write(u_h_, name)
                    attrs = hdf5_handle.attributes(name)
                    attrs["t"] = t
                    attrs["x_c"] = x_c
                    attrs["y_c"] = y_c
            else:
                hdf5_handle.write(u_h_, name)

        t += 1.0 / k_inv.values()
        i += 1

        # Non-linear term in right hand side, must re-assemble.
        assemble(L_form, tensor=b)
        bc.apply(b)
        solver.solve(u_h_.vector(), b)

        u_h.assign(u_h_)

    if write_xdmf:
        XDMFFile("output/%s/u_%s.xdmf" %
                 (str(id), str(i).zfill(4))).write(u_h_)
    if hdf5_handle is not None:
        name = "snapshots-%s-%s" % (id, str(i).zfill(4))
        if ts is not None:
            if (np.abs(t - ts) < DOLFIN_EPS_LARGE).any():
                hdf5_handle.write(u_h_, name)
        else:
            hdf5_handle.write(u_h_, name)

    return u_h


def generate_snapshots():
    h5 = HDF5File(mpi_comm_world(), "output/snapshots.h5", "w")

    # Discrete snapshot space on position.
    xs = ys = np.linspace(0.5, 1.0, 6)
    # Discrete snapshot space in time, used in method fkpp to select snapshots
    # in time.
    ts = np.linspace(0.0, 0.1, 11)

    from itertools import product
    begin("Generating snapshots M...")
    for i, (x_c, y_c) in enumerate(product(xs, ys)):
        info("M_%s: x_c = %.2f, y_c = %.2f, ts = %s" %
             (str(i).zfill(4), x_c, y_c, ts))
        fkpp(x_c, y_c, write_xdmf=False,
             hdf5_handle=h5, ts=ts, id=str(i).zfill(4))
    end()
    
    h5.close()


def example_solution():
    begin("Example FEM solution...")
    fkpp(0.55, 0.55, write_xdmf=True, id="fem_example")
    end()


if __name__ == "__main__":
    t = Timer("FEM solution")
    example_solution()
    del t
    list_timings(TimingClear_clear, [TimingType_wall])
    generate_snapshots()
