# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np
import scipy.linalg as linalg
from dolfin import *

from fkpp import *
from rapod import *

def fkpp_pod(V_R, x_c, y_c, V=V, write_xdmf=False, id="test", hdf5_handle=None, ts=None):
    u = TrialFunction(V)
    v = TestFunction(V)

    u_0.x_c = x_c
    u_0.y_c = y_c
    u_h = interpolate(u_0, V)

    a_form = a(u, v)
    L_form = L(u_h, v)

    bc = DirichletBC(V, Constant(0.0), all_boundary)

    # Assemble once, not a function of current solution.
    assembler = SystemAssembler(a_form, L_form, bc)
    A = Matrix()
    assembler.assemble(A)
    A_R = project(A, V_R)
    # Prefactorise matrix. In reality, this optimisation does very little to
    # the overall runtime of the POD procedure.
    timer = Timer("Dense Cholesky Factor")
    factors = linalg.cho_factor(A_R.array())
    del timer

    t = np.array([0.0])
    i = 0
    b = Vector()

    # To store next solution in time.
    u_h_ = Function(V)
    u_h_.assign(u_h)

    # Step forward in time.
    while t < T.values() - DOLFIN_EPS:
        if write_xdmf:
            timer = Timer("XDMF output")
            XDMFFile("output/%s/u_%s.xdmf" %
                     (str(id), str(i).zfill(4))).write(u_h)
            del timer
	    
        if hdf5_handle is not None:
            name = "snapshots-%s-%s" % (id, str(i).zfill(4))
            if ts is not None:
                if (np.abs(t - ts) < DOLFIN_EPS_LARGE).any():
                    hdf5_handle.write(u_h_, name)
                    attrs = hdf5_handle.attributes(name)
                    attrs["t"] = t
                    attrs["x_c"] = x_c
                    attrs["y_c"] = y_c
            else:
                hdf5_handle.write(u_h_, name)
        t += 1.0 / k_inv.values()
        i += 1

        # Non-linear term in right hand side, must re-assemble.
        # TODO: SystemAssembler.assemble(b) seems slower than using assemble?
        assemble(L_form, tensor=b)
        bc.apply(b)
        b_R = project(b, V_R)

        # TODO: Better to use PETSc? DB says LAPACK faster.
        timer = Timer("Dense Cholesky Solve")
        u_R_ = linalg.cho_solve(factors, b_R.array(), check_finite=False)
        del timer

        # Bring back to FE space.
        project(u_R_, V_R, tensor=u_h_.vector())

        # Update solution in FE space.
        u_h.assign(u_h_)

    if write_xdmf:
        XDMFFile("output/%s/u_%s.xdmf" %
                 (str(id), str(i).zfill(4))).write(u_h_)
    if hdf5_handle is not None:
        name = "snapshots-%s-%s" % (id, str(i).zfill(4))
        if ts is not None:
            if (np.abs(t - ts) < DOLFIN_EPS_LARGE).any():
                hdf5_handle.write(u_h_, name)
            else:
                hdf5_handle.write(u_h_, name)
    
    return u_h


def pod_solution():
    V_R, _, _ = read_basis("output/basis.h5", V, scale_with_eigenvalues=True)
   
    timings(TimingClear_clear, [TimingType_wall]) 
    t = Timer("POD Solution...")
    u_h_pod = fkpp_pod(V_R, 0.55, 0.55, write_xdmf=False, id="pod_example")
    del t
    end()
    
    list_timings(TimingClear_clear, [TimingType_wall])

    return u_h_pod


def rapod_solution():
    V_R, phis, eigenvalues = read_basis("output/basis.h5", V, scale_with_eigenvalues=False)
    reduced_mesh = sequential_reduction(phis, V, coarse_mesh, 1E-2) 
    V_N = FunctionSpace(reduced_mesh, "CG", 1)
    
    M = assemble_inner_product(V_N)
    V_RN, _ = reduce_basis(phis, V_N)
    
    orthogonalize(V_RN, M)
    scale_basis(V_RN, eigenvalues)
   
    timings(TimingClear_clear, [TimingType_wall]) 
    t = Timer("RAPOD Solution...")
    u_h_ra = fkpp_pod(V_RN, 0.55, 0.55, V=V_N, write_xdmf=True, id="rapod_example")
    del t
    list_timings(TimingClear_clear, [TimingType_wall])

    return u_h_ra


if __name__ == "__main__":
    timings(TimingClear_clear, [TimingType_wall]) 
    t = Timer("FEM solution...")
    u_h = fkpp(0.55, 0.55)
    del t
    list_timings(TimingClear_clear, [TimingType_wall])

    u_h_pod = pod_solution()
    error_pod_fem = errornorm(u_h, u_h_pod, "H1")/norm(u_h, "H1")
    print(error_pod_fem)
    
    u_h_rapod = rapod_solution()

    u_h_rapod_V = Function(V)
    LagrangeInterpolator().interpolate(u_h_rapod_V, u_h_rapod)
    
    error_rapod_pod = errornorm(u_h_pod, u_h_rapod_V, "H1")/norm(u_h_pod, "H1")
    print(error_rapod_pod)

    error_rapod_fem = errornorm(u_h, u_h_rapod_V, "H1")/norm(u_h, "H1")
    print(error_rapod_fem)
