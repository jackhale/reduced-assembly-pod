from __future__ import print_function

import numpy as np 
import pandas as pd
import xmltodict

import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("ticks")
sns.set_context("talk")

df = pd.read_json("output/study_three/results.json", orient='records').sort_index()

fig = plt.figure(figsize=(5, 4))
ax = plt.gca()
ax.plot(df.index.values + 1, df["number of cells"], 'o-', marker='x', markersize=10)
plt.xlabel('$i$')
plt.ylabel('number of cells')
plt.savefig("output/study_three/evolution.pdf", bbox_inches='tight')
