#### import the simple module from the paraview
from paraview.simple import *

def plot(input_filename, output_filename):
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    # create a new 'Xdmf3ReaderT'
    rapod_mesh_0xdmf = Xdmf3ReaderT(FileName=[input_filename])

    # Create a new 'Render View'
    renderView1 = CreateView('RenderView')
    renderView1.ViewSize = [1804, 1223]
    renderView1.AnnotationColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid = 'GridAxes3DActor'
    renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
    renderView1.StereoType = 0
    renderView1.Background = [1.0, 1.0, 1.0]

    # init the 'GridAxes3DActor' selected for 'AxesGrid'
    renderView1.AxesGrid.XTitleColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.YTitleColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.ZTitleColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.XLabelColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.YLabelColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.ZLabelColor = [0.0, 0.0, 0.0]

    # get layout
    layout1 = GetLayout()

    # place view in the layout
    layout1.AssignView(0, renderView1)

    # show data in view
    rapod_mesh_0xdmfDisplay = Show(rapod_mesh_0xdmf, renderView1)
    # trace defaults for the display properties.
    rapod_mesh_0xdmfDisplay.Representation = 'Surface'
    rapod_mesh_0xdmfDisplay.ColorArrayName = [None, '']
    rapod_mesh_0xdmfDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
    rapod_mesh_0xdmfDisplay.SelectOrientationVectors = 'None'
    rapod_mesh_0xdmfDisplay.ScaleFactor = 0.30000000000000004
    rapod_mesh_0xdmfDisplay.SelectScaleArray = 'None'
    rapod_mesh_0xdmfDisplay.GlyphType = 'Arrow'
    rapod_mesh_0xdmfDisplay.PolarAxes = 'PolarAxesRepresentation'
    rapod_mesh_0xdmfDisplay.ScalarOpacityUnitDistance = 0.5175103654941158
    rapod_mesh_0xdmfDisplay.GaussianRadius = 0.15000000000000002
    rapod_mesh_0xdmfDisplay.SetScaleArray = [None, '']
    rapod_mesh_0xdmfDisplay.ScaleTransferFunction = 'PiecewiseFunction'
    rapod_mesh_0xdmfDisplay.OpacityArray = [None, '']
    rapod_mesh_0xdmfDisplay.OpacityTransferFunction = 'PiecewiseFunction'

    # init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
    rapod_mesh_0xdmfDisplay.OSPRayScaleFunction.Points = [-0.7294652066596538, 0.0, 0.5, 0.0, 1.755860547598631, 1.0, 0.5, 0.0]

    # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
    rapod_mesh_0xdmfDisplay.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
    rapod_mesh_0xdmfDisplay.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
    rapod_mesh_0xdmfDisplay.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
    rapod_mesh_0xdmfDisplay.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

    # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
    rapod_mesh_0xdmfDisplay.ScaleTransferFunction.Points = [-0.7294652066596538, 0.0, 0.5, 0.0, 1.755860547598631, 1.0, 0.5, 0.0]

    # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
    rapod_mesh_0xdmfDisplay.OpacityTransferFunction.Points = [-0.7294652066596538, 0.0, 0.5, 0.0, 1.755860547598631, 1.0, 0.5, 0.0]

    # reset view to fit data
    renderView1.ResetCamera()

    #changing interaction mode based on data extents
    renderView1.InteractionMode = '2D'
    renderView1.CameraPosition = [1.5, 1.5, 10000.0]
    renderView1.CameraFocalPoint = [1.5, 1.5, 0.0]

    # Hide orientation axes
    renderView1.OrientationAxesVisibility = 0

    # change representation type
    rapod_mesh_0xdmfDisplay.SetRepresentationType('Surface With Edges')
    # current camera placement for renderView1
    renderView1.InteractionMode = '2D'
    renderView1.CameraPosition = [1.8157149006301125, 1.4255054728850298, 10000.0]
    renderView1.CameraFocalPoint = [1.8157149006301125, 1.4255054728850298, 0.0]
    renderView1.CameraParallelScale = 1.65 

    # save screenshots
    renderView1.ViewSize = [400, 300]
    SaveScreenshot(output_filename, magnification=5, quality=100, view=renderView1, ImageResolution=(2048, 2048))

    Delete(renderView1)
    del renderView1

    Delete(rapod_mesh_0xdmfDisplay)
    del rapod_mesh_0xdmfDisplay

import os
path = os.path.join(os.getcwd(), 'output/study_two/')
fs = [path + f for f in os.listdir(path) if (f.endswith('.xdmf') and "mesh" in f)]
for f in fs:
    f_output = os.path.splitext(f)[0] + ".png"
    print f_output
    plot(f, f_output) 
