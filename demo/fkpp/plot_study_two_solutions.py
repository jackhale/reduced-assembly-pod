#### import the simple module from the paraview
from paraview.simple import *

def plot(input_filename, output_filename):
    #### disable automatic camera reset on 'Show'
    paraview.simple._DisableFirstRenderCameraReset()

    # Create a new 'Render View'
    renderView1 = CreateView('RenderView')
    renderView1.ViewSize = [1300, 1196]
    renderView1.AnnotationColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid = 'GridAxes3DActor'
    renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
    renderView1.StereoType = 0
    renderView1.Background = [1.0, 1.0, 1.0]

    # init the 'GridAxes3DActor' selected for 'AxesGrid'
    renderView1.AxesGrid.XTitleColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.YTitleColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.ZTitleColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.XLabelColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.YLabelColor = [0.0, 0.0, 0.0]
    renderView1.AxesGrid.ZLabelColor = [0.0, 0.0, 0.0]

    # get layout
    layout1 = GetLayout()

    # place view in the layout
    layout1.AssignView(0, renderView1)

    # create a new 'Xdmf3ReaderT'
    rapod_0_ = Xdmf3ReaderT(FileName=[input_filename])
    rapod_0_.PointArrays = ['snapshots']

    # get animation scene
    animationScene1 = GetAnimationScene()

    # update animation scene based on data timesteps
    animationScene1.UpdateAnimationUsingDataTimeSteps()

    # get color transfer function/color map for 'snapshots'
    snapshotsLUT = GetColorTransferFunction('snapshots')

    # show data in view
    rapod_0_Display = Show(rapod_0_, renderView1)
    # trace defaults for the display properties.
    rapod_0_Display.Representation = 'Surface'
    rapod_0_Display.ColorArrayName = ['POINTS', 'snapshots']
    rapod_0_Display.LookupTable = snapshotsLUT
    rapod_0_Display.OSPRayScaleArray = 'snapshots'
    rapod_0_Display.OSPRayScaleFunction = 'PiecewiseFunction'
    rapod_0_Display.SelectOrientationVectors = 'None'
    rapod_0_Display.ScaleFactor = 0.30000000000000004
    rapod_0_Display.SelectScaleArray = 'snapshots'
    rapod_0_Display.GlyphType = 'Arrow'
    rapod_0_Display.PolarAxes = 'PolarAxesRepresentation'
    rapod_0_Display.ScalarOpacityUnitDistance = 0.5175103654941158
    rapod_0_Display.GaussianRadius = 0.15000000000000002
    rapod_0_Display.SetScaleArray = ['POINTS', 'snapshots']
    rapod_0_Display.ScaleTransferFunction = 'PiecewiseFunction'
    rapod_0_Display.OpacityArray = ['POINTS', 'snapshots']
    rapod_0_Display.OpacityTransferFunction = 'PiecewiseFunction'

    # init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
    rapod_0_Display.OSPRayScaleFunction.Points = [-0.7294652066596538, 0.0, 0.5, 0.0, 1.755860547598631, 1.0, 0.5, 0.0]

    # init the 'PolarAxesRepresentation' selected for 'PolarAxes'
    rapod_0_Display.PolarAxes.PolarAxisTitleColor = [0.0, 0.0, 0.0]
    rapod_0_Display.PolarAxes.PolarAxisLabelColor = [0.0, 0.0, 0.0]
    rapod_0_Display.PolarAxes.LastRadialAxisTextColor = [0.0, 0.0, 0.0]
    rapod_0_Display.PolarAxes.SecondaryRadialAxesTextColor = [0.0, 0.0, 0.0]

    # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
    rapod_0_Display.ScaleTransferFunction.Points = [-0.7294652066596538, 0.0, 0.5, 0.0, 1.755860547598631, 1.0, 0.5, 0.0]

    # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
    rapod_0_Display.OpacityTransferFunction.Points = [-0.7294652066596538, 0.0, 0.5, 0.0, 1.755860547598631, 1.0, 0.5, 0.0]

    # reset view to fit data
    renderView1.ResetCamera()

    #changing interaction mode based on data extents
    renderView1.InteractionMode = '2D'
    renderView1.CameraPosition = [1.5, 1.5, 10000.0]
    renderView1.CameraFocalPoint = [1.5, 1.5, 0.0]

    # show color bar/color legend
    rapod_0_Display.SetScalarBarVisibility(renderView1, True)

    # Hide orientation axes
    renderView1.OrientationAxesVisibility = 0

    # get color legend/bar for snapshotsLUT in view renderView1
    snapshotsLUTColorBar = GetScalarBar(snapshotsLUT, renderView1)

    # change scalar bar placement
    snapshotsLUTColorBar.Position = [0.8346153846153846, 0.2707357859531772]
    snapshotsLUTColorBar.Position2 = [0.12, 0.43]

    # Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
    snapshotsLUT.ApplyPreset('BuGn', True)

    # Rescale transfer function
    snapshotsLUT.RescaleTransferFunction(0.0, 1.0)

    # get opacity transfer function/opacity map for 'snapshots'
    snapshotsPWF = GetOpacityTransferFunction('snapshots')

    # Rescale transfer function
    snapshotsPWF.RescaleTransferFunction(0.0, 1.0)

    # Properties modified on snapshotsLUTColorBar
    snapshotsLUTColorBar.Title = 'solution'

    # Properties modified on snapshotsLUTColorBar
    snapshotsLUTColorBar.AutomaticLabelFormat = 0
    snapshotsLUTColorBar.LabelFormat = '%-#6.1g'

    # Properties modified on snapshotsLUTColorBar
    snapshotsLUTColorBar.LabelFormat = '%-#1.1g'

    # Properties modified on snapshotsLUTColorBar
    snapshotsLUTColorBar.RangeLabelFormat = '%4.1e'

    # Properties modified on snapshotsLUTColorBar
    snapshotsLUTColorBar.RangeLabelFormat = '%.1'

    # Properties modified on snapshotsLUTColorBar
    snapshotsLUTColorBar.RangeLabelFormat = '%4.1g'

    # current camera placement for renderView1
    renderView1.InteractionMode = '2D'
    renderView1.CameraPosition = [1.8157149006301125, 1.4255054728850298, 10000.0]
    renderView1.CameraFocalPoint = [1.8157149006301125, 1.4255054728850298, 0.0]
    renderView1.CameraParallelScale = 1.65 

    # save screenshots
    renderView1.ViewSize = [400, 300]
    SaveScreenshot(output_filename, magnification=5, quality=100, view=renderView1, ImageResolution=(2048, 2048))

    Delete(renderView1)
    del renderView1

    Delete(rapod_0_)
    del rapod_0_

import os
path = os.path.join(os.getcwd(), 'output/study_two/')
fs = [path + f for f in os.listdir(path) if (f.endswith('.xdmf') and "solution" in f)]
for f in fs:
    f_output = os.path.splitext(f)[0] + ".png"
    print f_output
    plot(f, f_output)
