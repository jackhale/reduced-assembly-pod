from __future__ import print_function

import numpy as np 

from dolfin import *

from fkpp import *
from rapod import read_snapshots

mesh = Mesh()

for i in range(0, 3):
    f = XDMFFile(mpi_comm_world(), "output/study_two/rapod_mesh_{}.xdmf".format(i))
    f.read(mesh)
    
    V_R = FunctionSpace(mesh, "CG", 1)
    us = read_snapshots("output/study_two/rapod_{}.h5".format(i), V_R)
    for j in range(0, 3):
        XDMFFile("output/study_two/rapod_solution_{}_{}.xdmf".format(i, j)).write(us[j])
   
us = read_snapshots("output/study_two/fem.h5", V)
for j in range(0, 3):
    XDMFFile("output/study_two/fem_solution_{}.xdmf".format(j)).write(us[j])
   
us = read_snapshots("output/study_two/pod.h5", V)
for j in range(0, 3):
    XDMFFile("output/study_two/pod_solution_{}.xdmf".format(j)).write(us[j])
