# Copyright (C) 2016 Jack S. Hale.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np
import pandas as pd

from dolfin import *
from rapod import *

from fkpp import *
from fkpp_pod import fkpp_pod

def write_timings(filename):
    if mpi_comm_world().rank == 0:
        t = timings(TimingClear_clear, [TimingType_wall])
        File(mpi_comm_self(), filename) << t


def clear_timings():
    t = timings(TimingClear_clear, [TimingType_wall])


def main():
    results = {} 
    
    # Baseline FEM result
    clear_timings()
    t = Timer("Total")
    u_h = fkpp(0.55, 0.55)
    del t
    write_timings("output/study_one/fem.xml")

    u_h_norm = norm(u_h, "H1")

    V_R, phis, eigenvalues = read_basis("output/basis.h5", V, scale_with_eigenvalues=True)
 
    # Baseline standard Galerkin POD result
    clear_timings()
    t = Timer("Total")
    u_h_pod = fkpp_pod(V_R, 0.55, 0.55)
    del t
    write_timings("output/study_one/pod.xml")

    u_h_pod_V = interpolate(u_h_pod, V)
    result = {}
    result["error"] = errornorm(u_h, u_h_pod, "H1")/u_h_norm
    result["tolerance"] = 0.0 
    results["pod"] = result   
 
    V_R, phis, eigenvalues = read_basis("output/basis.h5", V, scale_with_eigenvalues=False)
    # RAPOD loop over tolerances in integration    
    tolerances = np.logspace(-1, -5, num=5) 
    for i, tolerance in enumerate(tolerances):
        result = {}
        reduced_mesh = Mesh()
        f = XDMFFile(mpi_comm_world(), "output/study_one/rapod_mesh_{}.xdmf".format(i))
        f.read(reduced_mesh)
        f.close()

        V_N = FunctionSpace(reduced_mesh, "CG", 1)
        M = assemble_inner_product(V_N)
        V_RN, _ = reduce_basis(phis, V_N)
        orthogonalize(V_RN, M)
        scale_basis(V_RN, eigenvalues)

        clear_timings()
        t = Timer("Total")
        u_h_ra = fkpp_pod(V_RN, 0.55, 0.55, V=V_N)
        del t
        write_timings("output/study_one/rapod_{}.xml".format(i))
        
        u_h_ra_V = Function(V)
        LagrangeInterpolator().interpolate(u_h_ra_V, u_h_ra)
        result["error"] = errornorm(u_h, u_h_ra_V, "H1")/u_h_norm
        result["tolerance"] = tolerance
        results["rapod_{}".format(i)] = result
    
    if mpi_comm_world().rank == 0:
        df = pd.DataFrame.from_dict(results)
        df.to_json(path_or_buf="output/study_one/results.json")


if __name__ == "__main__":
    main()
