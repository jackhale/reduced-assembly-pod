# Copyright (C) 2016 Jack S. Hale.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np
import pandas as pd

from dolfin import *
from rapod import *

from fkpp import *
from fkpp_pod import fkpp_pod

def main():
    V_R, phis, eigenvalues = read_basis("output/basis.h5", V, scale_with_eigenvalues=False)
    # RAPOD loop over tolerances in integration    
    tolerances = np.logspace(-1, -5, num=5) 
    for i, tolerance in enumerate(tolerances):
        result = {}
        reduced_mesh = sequential_reduction(phis, V, coarse_mesh, tolerance)
        XDMFFile("output/study_one/rapod_mesh_{}.xdmf".format(i)).write(reduced_mesh)


if __name__ == "__main__":
    main()
