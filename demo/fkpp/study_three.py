# Copyright (C) 2016 Jack S. Hale.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np
import pandas as pd

from dolfin import *
from rapod import *

from fkpp import *
from fkpp_pod import fkpp_pod

def main():
    # Write out original modes
    V_R, phis, eigenvalues = read_basis("output/basis.h5", V, scale_with_eigenvalues=False)
    
    for i, phi in enumerate(phis):
        XDMFFile("output/study_three/phi_original_{}.xdmf".format(str(i).zfill(4))).write(phi)
   
    # Write out reduction meshes and interpolate modes for one tolerance.
    tolerance = 1E-2
    reduced_mesh, phis_V_R = sequential_reduction(phis, V, coarse_mesh, tolerance, return_intermediate_phis=True)

    num_cells = []
    for i, phi in enumerate(phis_V_R):
        XDMFFile("output/study_three/phi_reduced_{}.xdmf".format(str(i).zfill(4))).write(phi)
        num_cells.append(phi.function_space().mesh().num_cells())

    # Write out data.
    df = pd.DataFrame({"eigenvalues": eigenvalues, "number of cells": num_cells})
    df.to_json("output/study_three/results.json")

if __name__ == "__main__":
    main()
