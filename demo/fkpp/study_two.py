# Copyright (C) 2016 Jack S. Hale.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np

from dolfin import *
from rapod import *

from fkpp import *
from fkpp_pod import fkpp_pod


def main():
    ts = np.array([0, 0.05, 0.1])

    h5 = HDF5File(mpi_comm_world(), "output/study_two/fem.h5", "w")
    u_h = fkpp(0.55, 0.55, ts=ts, hdf5_handle=h5)
    h5.close()

    V_R, phis, eigenvalues = read_basis("output/basis.h5", V, scale_with_eigenvalues=True)

    h5 = HDF5File(mpi_comm_world(), "output/study_two/pod.h5", "w")
    u_h_pod = fkpp_pod(V_R, 0.55, 0.55, ts=ts, hdf5_handle=h5)
    h5.close()

    tolerances = np.array([1E-1, 5E-2, 1E-2])
    print(tolerances)

    for i, tolerance in enumerate(tolerances):
        h5 = HDF5File(mpi_comm_world(), "output/study_two/rapod_{}.h5".format(i), "w")
        reduced_mesh = sequential_reduction(phis, V, coarse_mesh, tolerance)
        XDMFFile("output/study_two/rapod_mesh_{}.xdmf".format(i)).write(reduced_mesh)
        V_N = FunctionSpace(reduced_mesh, "CG", 1)

        M = assemble_inner_product(V_N)
        V_RN, _ = reduce_basis(phis, V_N)
        orthogonalize(V_RN, M)
        scale_basis(V_RN, eigenvalues)

        u_h_ra = fkpp_pod(V_RN, 0.55, 0.55, V=V_N, ts=ts, hdf5_handle=h5)

        u_h_ra_V = interpolate(u_h_ra, V)
        e_V = Function(V)
        e_V.vector()[:] = u_h_ra_V.vector() - u_h.vector()
        e_V.vector().abs()
        e_V.rename("snapshots", "snapshots")
        
        XDMFFile("output/study_two/rapod_error_fem_{}.xdmf".format(i)).write(e_V)

        e_V.vector()[:] = u_h_ra_V.vector() - u_h_pod.vector()
        e_V.vector().abs()
        XDMFFile("output/study_two/rapod_error_pod_{}.xdmf".format(i)).write(e_V)

        h5.close()
    

if __name__ == "__main__":
    main()
