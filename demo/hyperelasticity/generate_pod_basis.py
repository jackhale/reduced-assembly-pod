# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

from rapod import *

if __name__ == "__main__":
    from hyperelasticity import V, assemble_inner_product
    M = assemble_inner_product()
    generate_pod_basis_from_snapshots("output/snapshots.h5", "output/basis.h5", V, inner_product=M, cut_off=0.9999)
