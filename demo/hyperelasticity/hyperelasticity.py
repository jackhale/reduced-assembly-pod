# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np
from dolfin import *

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["optimize"] = True

E = Constant(10.0)
nu = Constant(0.3)
mu = Constant(E/(2.0*(1.0 + nu)))
lmbda = Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)))

sigma = Constant(0.15)
traction = Expression(("0.0", "0.0", "-6.0*t*exp(-((x[0] - x_c)*(x[0] - x_c) + (x[1] - y_c)*(x[1] - y_c))/(sigma*sigma))"), t=0.0, x_c=0.0, y_c=0.0, sigma=sigma, degree=3)

nd = 32
snapshot_mesh = UnitCubeMesh(mpi_comm_world(), nd, nd, nd)
coarse_mesh = UnitCubeMesh(mpi_comm_world(), 2, 2, 2)

V = VectorFunctionSpace(snapshot_mesh, "CG", 1)

all_boundary = CompiledSubDomain("on_boundary")
bottom_boundary = CompiledSubDomain("on_boundary && near(x[2], 0.0)")
top_boundary = CompiledSubDomain("on_boundary && near(x[2], 1.0)")

boundary_subdomains = MeshFunction("size_t", snapshot_mesh, snapshot_mesh.topology().dim() - 1)
boundary_subdomains.set_all(0)
bottom_boundary.mark(boundary_subdomains, 1)
top_boundary.mark(boundary_subdomains, 2)
ds = ds(subdomain_data=boundary_subdomains)

def assemble_inner_product(V=V):
    u = TrialFunction(V)
    v = TestFunction(V)

    a = inner(u, v) * dx
    M = assemble(a)

    return M

def Pi(u_, f, dx=dx, ds=ds):
    I = Identity(3)
    F = I + grad(u_)

    C = F.T*F

    Ic = tr(C)
    J = det(F)

    psi = (mu/2.0)*(Ic - 3) - mu*ln(J) + (lmbda/2)*(ln(J))**2
    Pi = psi*dx - inner(f, u_)*ds(2)

    return Pi


def hyperelasticity(x_c, y_c, id="test", V=V, ts=None, write_xdmf=False, hdf5_handle=None):
    u_ = Function(V)
    u = TrialFunction(V)
    v = TestFunction(V)

    traction.x_c = x_c
    traction.y_c = y_c

    Pi_form = Pi(u_, traction)
    F = derivative(Pi_form, u_, v)
    J = derivative(F, u_, u)

    bc = DirichletBC(V, Constant((0.0, 0.0, 0.0)), bottom_boundary)

    class HyperelasticityProblem(NonlinearProblem):
        def __init__(self, F, J, bcs):
            NonlinearProblem.__init__(self)
            self.Fform = F
            self.Jform = J
            self.bcs = bcs

            self.assembler = SystemAssembler(J, F, bcs)

        def form(self, A, P, b, x0):
            if A is not None:
                self.assembler.assemble(A, b, x0)
            else:
                self.assembler.assemble(b, x0)

        def F(self, b, x):
	    pass

        def J(self, A, x):
            pass

    problem = HyperelasticityProblem(F, J, bc)
    linear_solver = PETScKrylovSolver()
    PETScOptions.set("ksp_type", "gmres")
    PETScOptions.set("pc_type", "gamg")
    #PETScOptions.set("ksp_monitor_true_residual")
    PETScOptions.set("ksp_divtol", 1E10)
    PETScOptions.set("ksp_rtol", 1E-7)
    PETScOptions.set("ksp_atol", 1E-13)
    PETScOptions.set("snes_rtol", 1E-6)
    PETScOptions.set("snes_atol", 1E-13)
    #PETScOptions.set("ksp_view")
    linear_solver.set_from_options()
    solver = NewtonSolver(mpi_comm_world(), linear_solver, PETScFactory.instance())

    continuation = np.linspace(0, 1.0, 11)
    # Continuation in loading (pseudo-time).
    for i, t in enumerate(continuation):
        traction.t = t
        solver.solve(problem, u_.vector())

        # Output results from previous timestep
        if write_xdmf:
            XDMFFile("output/%s/u_%s.xdmf" %
                     (str(id), str(i).zfill(4))).write(u_)
        if hdf5_handle is not None:
            name = "snapshots-%s-%s" % (id, str(i).zfill(4))
            if ts is not None:
                if (np.abs(t - ts) < DOLFIN_EPS_LARGE).any():
                    hdf5_handle.write(u_, name)
                    attrs = hdf5_handle.attributes(name)
                    attrs["t"] = t
                    attrs["x_c"] = x_c
                    attrs["y_c"] = y_c
            else:
                hdf5_handle.write(u_, name)

    return u_


def generate_snapshots():
    h5 = HDF5File(mpi_comm_world(), "output/snapshots.h5", "w")

    # Discrete snapshot space on position of surface traction on top
    # surface of cube.
    xs = ys = np.linspace(0.3, 0.6, 4)
    # Discrete snapshot space in pseudo-time, used in solver to select
    # snapshots in time.
    ts = np.linspace(0.0, 1.0, 11)

    from itertools import product
    begin("Generating snapshots M...")
    for i, (x_c, y_c) in enumerate(product(xs, ys)):
        info("M_%s: x_c = %.2f, y_c = %.2f, ts = %s" %
             (str(i).zfill(4), x_c, y_c, ts))
        hyperelasticity(x_c, y_c, write_xdmf=False,
             hdf5_handle=h5, ts=ts, id=str(i).zfill(4))
    end()

    h5.close()


def example_solution():
    begin("Example FEM solution...")
    hyperelasticity(0.55, 0.55, write_xdmf=True, id="fem_example")
    end()


if __name__ == "__main__":
    t = Timer("FEM solution")
    example_solution()
    del t
    exit()
    list_timings(TimingClear_clear, [TimingType_wall])
    generate_snapshots()
