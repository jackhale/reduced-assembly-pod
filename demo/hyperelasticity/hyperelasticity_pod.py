# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np
import scipy.linalg as linalg
from dolfin import *

from hyperelasticity import *
from rapod import *

def hyperelasticity_pod(V_R, x_c, y_c, V=V, write_xdmf=False, id="test", hdf5_handle=None, ts=None):
    u_ = Function(V)
    u = TrialFunction(V)
    v = TestFunction(V)

    traction.x_c = x_c
    traction.y_c = y_c

    mesh = V.mesh()
    boundary_subdomains = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    bottom_boundary.mark(boundary_subdomains, 1)
    top_boundary.mark(boundary_subdomains, 2)
    dx = Measure("cell", domain=mesh) 
    ds = Measure("exterior_facet", domain=mesh, subdomain_data=boundary_subdomains)
    
    Pi_form = Pi(u_, traction, dx=dx, ds=ds)
    F = derivative(Pi_form, u_, v)
    J = derivative(F, u_, u)

    bc = DirichletBC(V, Constant((0.0, 0.0, 0.0)), bottom_boundary)

    class HyperelasticityProblem(NonlinearProblem):
        def __init__(self, F, J, bcs):
            NonlinearProblem.__init__(self)
            self.Fform = F
            self.Jform = J
            self.bcs = bcs

            self.assembler = SystemAssembler(J, F, bcs)

        def form(self, A, P, b, x0):
            if A is not None:
                self.assembler.assemble(A, b, x0)
            else:
                self.assembler.assemble(b, x0)

        def F(self, b, x):
            pass

        def J(self, A, x):
            pass
    
    problem = HyperelasticityProblem(F, J, bc)
    A = Matrix()
    b = Vector()
    x = u_.vector() 
    du = x.copy()

    continuation = np.linspace(0, 1.0, 11)
    for i, t in enumerate(continuation):
        traction.t = t
        problem.form(None, None, b, x)
        
        b_R = project(b, V_R)
        residual = np.linalg.norm(b_R)

        # Newton loop
        while residual > 1E-10: 
            problem.form(A, None, b, x)

            A_R = project(A, V_R)
            b_R = project(b, V_R)
            
            timer = Timer("Dense Cholesky Factor")
            factors = linalg.cho_factor(A_R.array())
            del timer
            
            timer = Timer("Dense Cholesky Solve")
            du_R = linalg.cho_solve(factors, b_R.array(), check_finite=False)
            del timer

            du.zero()
            project(du_R, V_R, tensor=du)
            x -= du

            problem.form(None, None, b, x)
            b_R = project(b, V_R)
            residual = np.linalg.norm(b_R)

        if write_xdmf:
            timer = Timer("XDMF output")
            XDMFFile("output/%s/u_%s.xdmf" %
                     (str(id), str(i).zfill(4))).write(u_) 
    
    return u_


def pod_solution():
    V_R, _, _ = read_basis("output/basis.h5", V, scale_with_eigenvalues=True)
   
    timings(TimingClear_clear, [TimingType_wall]) 
    t = Timer("POD Solution...")
    u_h_pod = hyperelasticity_pod(V_R, 0.55, 0.55, write_xdmf=False, id="pod_example")
    del t
    end()
    
    list_timings(TimingClear_clear, [TimingType_wall])

    return u_h_pod


def rapod_solution():
    V_R, phis, eigenvalues = read_basis("output/basis.h5", V, scale_with_eigenvalues=False)
    norm_phis = []
    V_S = FunctionSpace(snapshot_mesh, "CG", 1)
    v = TestFunction(V_S)
    for phi in phis:
        norm_phi = Function(V_S)
        assemble(sqrt(inner(phi, phi))*v*dP, tensor=norm_phi.vector())
        norm_phis.append(norm_phi)

    reduced_mesh = sequential_reduction(norm_phis, V, coarse_mesh, 1E-2) 
    V_N = FunctionSpace(reduced_mesh, V.ufl_element())
    
    M = assemble_inner_product(V_N)
    V_RN, _ = reduce_basis(phis, V_N)
    
    orthogonalize(V_RN, M)
    scale_basis(V_RN, eigenvalues)
   
    timings(TimingClear_clear, [TimingType_wall]) 
    t = Timer("RAPOD Solution...")
    u_h_ra = hyperelasticity_pod(V_RN, 0.55, 0.55, V=V_N, write_xdmf=False, id="rapod_example")
    del t
    list_timings(TimingClear_clear, [TimingType_wall])

    return u_h_ra


if __name__ == "__main__":
    timings(TimingClear_clear, [TimingType_wall]) 
    t = Timer("FEM solution...")
    u_h = hyperelasticity(0.55, 0.55)
    del t
    list_timings(TimingClear_clear, [TimingType_wall])

    u_h_pod = pod_solution()
    error_pod_fem = errornorm(u_h, u_h_pod, "L2")/norm(u_h, "L2")
    print(error_pod_fem)

    u_h_rapod = rapod_solution()

    u_h_rapod_V = Function(V)
    LagrangeInterpolator().interpolate(u_h_rapod_V, u_h_rapod)
    
    error_rapod_pod = errornorm(u_h_pod, u_h_rapod_V, "L2")/norm(u_h_pod, "L2")
    print(error_rapod_pod)

    error_rapod_fem = errornorm(u_h, u_h_rapod_V, "L2")/norm(u_h, "L2")
    print(error_rapod_fem)
