from __future__ import print_function

import numpy as np 
import pandas as pd
import xmltodict

from rapod import *
from hyperelasticity import V

import matplotlib.pyplot as plt
import seaborn as sns

V_R, phis, eigenvalues = read_basis("output/basis_all.h5", V)

sns.set_style("ticks")
print(sns.axes_style())
sns.set_context('talk')

fig = plt.figure(figsize=(5, 4))
ax = plt.gca()
ax.plot(np.arange(1, len(eigenvalues) + 1), eigenvalues, '-', marker='x')
plt.xlabel('$i$')
plt.yscale('log')
plt.ylabel('$\sigma_i$')
plt.legend(loc='lower right')
plt.savefig("output/study_four/spectrum.pdf", bbox_inches='tight')
