# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np
import h5py
from progressbar import ProgressBar

from dolfin import *
from petsc4py import PETSc
from slepc4py import SLEPc

import rapod
from utilities import mat, vec 

def read_basis(basis_file, V, scale_with_eigenvalues=True):
    begin("Loading basis from file %s..." % basis_file)
    import h5py
    h5 = HDF5File(mpi_comm_world(), basis_file, "r")
    h5_py = h5py.File(basis_file, "r", driver="mpio",
                      comm=mpi_comm_world().tompi4py())

    basis = h5_py["/"]
    basis_size = len(basis.items())
    
    phis = []
    eigenvalues = []
    V_R = SLEPc.BV().create()
    V_R.setType('svec')
    V_R.setSizesFromVec(vec(Function(V)), basis_size)
   
    for i, group in enumerate(basis.itervalues()):
        tmp = Function(V, name="mode")
        h5.read(tmp, group.name[1:].encode('ascii'))
        attrs = h5.attributes(group.name[1:].encode('ascii'))

        eigenvalues.append(attrs["eigenvalue"])
        phis.append(tmp) 
        V_R.insertVec(i, vec(tmp))

    if scale_with_eigenvalues:
        rapod.pod.scale_basis(V_R, eigenvalues)
    
    info("Loaded %i basis functions." % basis_size)
    
    h5.close()
    h5_py.close()
    end()

    return (V_R, phis, np.array(eigenvalues))


def write_basis(basis_file, V, V_R, eigenvalues, write_xdmf=False):
    begin("Writing basis to file...")
    h5 = HDF5File(mpi_comm_world(), basis_file, "w")
    phi_i = Function(V)
    for i in range(0, V_R.getActiveColumns()[1]):
        phi = V_R.getColumn(i)

        phi.copy(result=vec(phi_i))

        name = "basis-%s" % (str(i).zfill(4))

        if write_xdmf:
            XDMFFile("output/basis/%s.xdmf" % name).write(phi_i) 

        h5.write(phi_i, name)
        attrs = h5.attributes(name)
        attrs["eigenvalue"] = eigenvalues[i]
        V_R.restoreColumn(i, phi)

    h5.close()
    end()


def read_snapshots(snapshot_file, V):
    h5 = HDF5File(mpi_comm_world(), snapshot_file, "r") 
    h5_py = h5py.File(snapshot_file, "r", driver="mpio",
                      comm=mpi_comm_world().tompi4py())

    num_snapshots = len(h5_py.items())
    # Load in snapshots
    # TODO: Too memory intensive?
    us = []
    with ProgressBar(max_value=num_snapshots) as progress:
        for i, group in enumerate(h5_py.itervalues()):
            u = Function(V)
            # DOLFIN SWIG wrappers won't pass utf8 strings.
            h5.read(u, group.name[1:].encode('ascii'))
            u.rename("snapshots", "snapshots")
            us.append(u)
            progress.update(i)

    info("Finished loading %i snapshots." % num_snapshots)
    h5.close()
    h5_py.close()

    return us 
