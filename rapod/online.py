# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

from dolfin import *
import petsc4py.PETSc as PETSc

from utilities import mat, vec

def project(*args, **kwargs):
    import dolfin
    import slepc4py
    import numpy

    V = args[1]
    if not isinstance(V, slepc4py.SLEPc.BV):
        # Pass through to usual dolfin project if anything else.
        dolfin.project(*args, **kwargs)

    A = args[0]
    if isinstance(A, dolfin.cpp.la.Matrix):
        # Project matrix from FE space to reduced space.
        t = Timer("FE Matrix to POD Matrix")
        A_R = V.matProject(mat(A), V)
        return PETScMatrix(A_R)
    elif isinstance(A, dolfin.cpp.la.Vector):
        # Project vector from FE space to reduced space.
        t = Timer("FE Vector to POD Vector")
        b_R = V.dotVec(vec(A))
        return PETScVector(b_R)
    elif isinstance(A, numpy.ndarray):
        # Project vector from reduced space to FE space.
        tensor = kwargs.pop("tensor", None)
        if tensor is None:
            raise ValueError(
                "For performance reasons you should pass the tensor kwarg with an appropriately sized dolfin.Vector to store the result of project()")

        t = Timer("POD Vector to FE Vector")
        V.multVec(1.0, 0.0, vec(tensor), A)
        return
    else:
        raise TypeError(
            "Don't know how to project object of type %s" % type(A))

    raise RuntimeError("Should never get here.")
