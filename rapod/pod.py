# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np

from dolfin import *
from petsc4py import PETSc
from slepc4py import SLEPc
from progressbar import ProgressBar

from input_output import read_snapshots, write_basis
from utilities import mat, vec

def generate_pod_basis_from_snapshots(snapshot_file, basis_file, V, inner_product=None, **kwargs):
    us = read_snapshots(snapshot_file, V)
    gram = construct_gram_matrix(us) 
    eps = solve_eigenvalue_problem(gram)
    V_R, eigenvalues = generate_pod_basis(eps, us, V, **kwargs)

    if inner_product is not None:
        orthogonalize(V_R, inner_product)
    else:
        info("No V-inner product supplied.")

    write_basis(basis_file, V, V_R, eigenvalues)


def orthogonalize(V_R, M):
    begin("Orthogonalising basis in V inner product space...")
    V_R.setMatrix(mat(M), False)
    V_R.orthogonalize()
    V_R.setMatrix(None, False)
    end()
    return V_R


def scale_basis(V_N, eigenvalues):
    _, truncate_basis_at = V_N.getActiveColumns()
    for i in range(0, truncate_basis_at):
        tmp = V_N.getColumn(i)
        tmp.scale(1.0 / np.sqrt(eigenvalues[i]))
        V_N.restoreColumn(i, tmp)


def generate_pod_basis(eps, us, V, cut_off=0.999):
    num_converged = eps.getConverged()
    truncate_basis_at = None
    
    eigenvalues = np.zeros(num_converged)
    for i in range(0, num_converged):
        eigenvalues[i] = eps.getEigenvalue(i).real 
    
    energy_total = np.sum(eigenvalues)
    for i in range(0, num_converged):
        energy_i = np.sum(eigenvalues[0:i])/energy_total
        if energy_i > cut_off:
            truncate_basis_at = i
            break
    
    info("Truncated basis at {} eigenvectors.".format(truncate_basis_at))

    bv = eps.getBV()

    V_R = SLEPc.BV().create()
    V_R.setSizesFromVec(vec(us[0]), truncate_basis_at)
    V_R.setType(bv.getType())
    V_R.setFromOptions()
    V_R.setActiveColumns(0, truncate_basis_at)

    # Working area.
    begin("Constructing POD basis...")
    phi_i = Function(V, name="basis_function")
    tmp = vec(us[0]).duplicate()
    us_vec = [vec(u) for u in us]
    for i in range(0, truncate_basis_at):
        w_bv = bv.getColumn(i)

        scatter, w = PETSc.Scatter.toAll(w_bv)
        scatter.begin(w_bv, w, PETSc.InsertMode.INSERT,
                      PETSc.ScatterMode.FORWARD)
        scatter.end(w_bv, w, PETSc.InsertMode.INSERT,
                    PETSc.ScatterMode.FORWARD)

        alpha = w.getArray()
        tmp.set(0.0)
        tmp.maxpy(alpha, us_vec)

        # Insert basis function into BV.
        V_R.insertVec(i, tmp)

        # NOTE: Always restore after getting from BV!
        bv.restoreColumn(i, w_bv)
    end()
    info("Finished constructing POD basis.")

    return V_R, eigenvalues


def construct_gram_matrix(us):
    num_snapshots = len(us)
    gram = PETSc.Mat().create()
    gram.setSizes([num_snapshots, num_snapshots])
    gram.setType('dense')
    gram.setUp()

    begin("Assembling Gramian matrix with Euclidean inner product...")
    with ProgressBar(max_value=num_snapshots) as progress:
        for i in range(num_snapshots):
            for j in range(num_snapshots):
                value = vec(us[i]).dot(vec(us[j]))
                gram.setValue(i, j, value)
                if (i != j):
                    gram.setValue(j, i, value)
            progress.update(i)

    gram.assemble()
    end()
    
    return gram


def solve_eigenvalue_problem(gram, dimension=50):
    begin("Solving eigenvalue problem...")
    eps = SLEPc.EPS().create()

    eps.setProblemType(SLEPc.EPS.ProblemType.HEP)
    eps.setType(SLEPc.EPS.Type.KRYLOVSCHUR)
    eps.setDimensions(50)
    eps.setWhichEigenpairs(SLEPc.EPS.Which.LARGEST_REAL)
    eps.setOperators(gram)
    eps.setFromOptions()
    eps.solve()
    end()
    info("Finished solving eigenvalue problem.") 

    return eps
