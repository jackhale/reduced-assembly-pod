# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import numpy as np

from dolfin import *
from slepc4py import SLEPc

from utilities import mat, vec

def estimate(mesh, f):
    interpolator = LagrangeInterpolator()
    
    U_DG = FunctionSpace(mesh, "DG", 0)
    v = TestFunction(U_DG)

    Q1 = FiniteElement("Quadrature", mesh.ufl_cell(), degree=1, quad_scheme="default")
    U_Q1 = FunctionSpace(mesh, Q1)
    f_Q1 = Function(U_Q1)
    interpolator.interpolate(f_Q1, f)
    I_1 = inner(f_Q1, v)*dx(mesh)

    Q2 = FiniteElement("Quadrature", mesh.ufl_cell(), degree=2, quad_scheme="default")
    U_Q2 = FunctionSpace(mesh, Q2)
    f_Q2 = Function(U_Q2)
    interpolator.interpolate(f_Q2, f)
    I_2 = inner(f_Q2, v)*dx(mesh)

    eta_1 = assemble(I_1)
    eta_2 = assemble(I_2)

    etas = eta_1 - eta_2
    etas.abs()
    etas = Function(U_DG, etas)

    return etas


def mark(alpha, indicators):
    etas = indicators.vector().array()
    indices = etas.argsort()[::-1]
    sorted = etas[indices]

    total = sum(sorted)
    fraction = alpha*total

    mesh = indicators.function_space().mesh()
    markers = CellFunction("bool", mesh, False)

    v = 0.0
    for i in indices:
        if v >= fraction:
            break
        markers[i] = True
        v += sorted[i]

    return markers


def sequential_reduction(phis, V, coarse_mesh, desired_error_tolerance, return_intermediate_phis=False):
    reduced_mesh = Mesh(coarse_mesh)
    V_R = FunctionSpace(reduced_mesh, "CG", 1)
    
    interpolator = LagrangeInterpolator()
    
    if return_intermediate_phis:
        phis_V_R = []

    for i, phi in enumerate(phis):
        exact = assemble(abs(phi)*dx(V.mesh()))
        
        phi_V_R = Function(V_R)
        interpolator.interpolate(phi_V_R, phi)

        approximate = assemble(abs(phi_V_R)*dx(reduced_mesh))
        error = np.abs(exact - approximate)/exact
        
        while error > desired_error_tolerance:
            eta = estimate(reduced_mesh, phi)
            markers = mark(0.005, eta)
            reduced_mesh = refine(reduced_mesh, markers)
            
            V_R = FunctionSpace(reduced_mesh, "CG", 1) 
            phi_V_R = Function(V_R)
            interpolator.interpolate(phi_V_R, phi)
            approximate = assemble(abs(phi_V_R)*dx(reduced_mesh))
            error = np.abs(exact - approximate)/exact

        if return_intermediate_phis:
            V_R = FunctionSpace(reduced_mesh, V.ufl_element())
            phi_V_R = interpolate(phi, V_R)
            phi_V_R.rename("mode", "mode")
            phis_V_R.append(phi_V_R)
    
    if return_intermediate_phis:
        return (reduced_mesh, phis_V_R)
    else:
        return reduced_mesh 


def greedy_reduction(phis, V, coarse_mesh, desired_error_tolerance, return_intermediate_modes=False):
    raise NotImplementedError
    reduced_mesh = Mesh(coarse_mesh)
    number_modes = len(phis)

    exacts = np.array([assemble(abs(phi)*dx(V.mesh())) for phi in phis])
    
    if return_intermediate_phis:
        phis_V_R = []
    
    max_tolerance = 1.0 
    while max_tolerance > desired_error_tolerance:
        # TODO: Mesh refinement deterministic? If so could cut down on the
        # copies of the current mesh here.
        meshes = [Mesh(reduced_mesh)]*number_modes
        for i, phi in enumerate(phis):
            eta = estimate(meshes[i], phi)
            markers = mark(0.005, eta)
            meshes[i] = refine(meshes[i], markers)
        
        approximates = np.array([assemble(abs(phi)*dx(mesh)) for phi, mesh in zip(phis, meshes)])
        errors = np.abs(exacts - approximates)/exacts
        
        highest = np.argmax(errors)
        reduced_mesh = meshes[highest]
        max_tolerance = errors[highest]
        
        if return_intermediate_phis:
            V_R = FunctionSpace(reduced_mesh, V.ufl_element())
            phi_V_R = interpolate(phi, V_R)
            phi_V_R.rename("mode", "mode")
            phis_V_R.append(phi_V_R)

    if return_intermediate_phis:
        return (reduced_mesh, phis_V_R)
    else:
        return reduced_mesh 


def reduce_basis(phis, V_N, M=None):
    truncate_basis_at = len(phis)
    V_RN = SLEPc.BV().create()
    V_RN.setSizesFromVec(vec(Function(V_N)), truncate_basis_at)
    V_RN.setType("svec")
    
    interpolator = LagrangeInterpolator()
    phis_N = []

    for i, phi in enumerate(phis):
        phi_N = Function(V_N)
        interpolator.interpolate(phi_N, phi)
        phi_N.rename("basis_function", "basis_function")
        V_RN.insertVec(i, vec(phi_N))
        phis_N.append(phi_N)

    # Reorthogonalise in M inner product.
    if M is not None:
        orthogonalize(V_RN, M)

    return V_RN, phis_N

