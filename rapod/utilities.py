# Copyright (C) 2016 Jack S. Hale, Elisa Schenone.
#
# This file is part of reduced-assembly-pod.
#
# reduced-assembly-pod is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# reduced-assembly-pod is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with reduced-assembly-pod. If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import sys
import ctypes

import dolfin as df


def vec(z):
    if isinstance(z, df.cpp.Function):
        return df.as_backend_type(z.vector()).vec()
    else:
        return df.as_backend_type(z).vec()


def mat(A):
    return df.as_backend_type(A).mat()


def prevent_MPI_output():
    # prevent output from rank > 0 processes
    if df.MPI.rank(df.mpi_comm_world()) > 0:
        df.info_blue("Turning off output from rank > 0 processes...")
        sys.stdout = open("/dev/null", "w")
        # and handle C++ as well
        libc = ctypes.CDLL("libc.so.6")
        stdout = libc.fdopen(1, "w")
        libc.freopen("/dev/null", "w", stdout)
